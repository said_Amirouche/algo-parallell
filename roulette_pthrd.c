/* la simulation avec Pthread mais avec N stable, le problem que j'ai c'est que le nombre des tours gagnantes ne cesse d'augmenter plus que le nombre des tours général

*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

const int MAX = 1<<10;	
#define NTHREADS      4
#define n     int(MAX)
#define ITERATIONS   n / NTHREADS

int getNumWins(int dist);
void showResults(int numSpins, int numWins);
int spinRed(int bet );


double  sum=0.0;
pthread_mutex_t sum_mutex;

void *do_work( void *tid) 
{
  int i, start, *mytid, end,dist;
  double x=0.0;
  
   mytid = (int *) tid;
  start = (*mytid * ITERATIONS);
  end = start + ITERATIONS;			// distribuer le nombre des opérations (Tours) à faire  pour les 4 threads 
dist = end-start-1;

  x = 0.0;
  x =  getNumWins(dist);   // Nombre des tours gagnante
 
  pthread_mutex_lock (&sum_mutex);
  sum = sum + x;
  pthread_mutex_unlock (&sum_mutex);
  pthread_exit(NULL);
}







int main(int argc, char *argv[]) {

  int i, start, tids[NTHREADS];
  pthread_t threads[NTHREADS];
  pthread_attr_t attr;
  clock_t startT, stopT; 
  startT = clock(); 
 

  int numSpins ;		
  int	numWins, localx;
	



  printf("Simulation d'une roulette américainne\n") ;
  printf("Nbr tours         Nbr tour gagnant        pourcentage de gagner\n");


		
  startT = clock(); 
  srand(time(NULL));

  pthread_mutex_init(&sum_mutex, NULL);
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  for (i=0; i<NTHREADS; i++) {
     	
    tids[i] = i;
    pthread_create(&threads[i], &attr, do_work, (void *) &tids[i]);
  }

  for (i=0; i<NTHREADS; i++) {
    pthread_join(threads[i], NULL);
  }


  showResults(n, sum);
 



  stopT= clock();	
	
  double t = (stopT - startT)/CLOCKS_PER_SEC;
  printf("le temps d'execution %f", t);

	

  return 0;
} 




int getNumWins(int dist) {

  static int wins;
  int spin;		
  int myBet = 10;
  int temp;

  wins = 0;	
	
  for (spin=0; spin<dist; spin++){
	
    temp = spinRed(myBet);
    if (temp>0) {
      wins++;
  }
	
  }

  return wins;
}  

int RandRange(int Min, int Max)
{
    int diff = Max-Min;
    return (int) (((double)(diff+1)/RAND_MAX) * rand() + Min);
}

int spinRed(int bet) {
  int payout;
  int slot;
slot  = RandRange(1, 38);  

  if (slot <= 18)
    payout = bet;	
  else if (slot <= 36) 
    payout = -bet;	
  else 
    payout = -(bet/2); 
  return payout;
} 



void showResults(int numSpins, int numWins){

  double percent = 100.0* (double)numWins/(double)n;
	
  printf("%d                       %d                          %f\n",numSpins,numWins,percent);
} 