
/* 	Une roulette américainne a 38 cases , 18 sont rouges et 18 sont noire et 2 sont verts
	Quand une personne parit soit sur le rouge ou le noir, la probabilité de gagner est de 18/38 voir 47.38%

	La simulation avec MPI et OPENMP a comme concept de distribuer le nombre tour entre les differents processus et partagé ce nombre déja distribué sur plusieurs threads
*/
#include <mpi.h>
#include <stdio.h>

#include <time.h>
#include <omp.h>
//#include <iomanip>
    
const int nThreads = 4; 


const int MAX = 1<<30;	


int getNumWins(int numSpins, unsigned int seed);
void showResults(int numSpins, int numWins);
int spinRed(int bet, unsigned int* seed);
int rand_rIntBetween(int low, int hi, unsigned int* seed) {
	return rand_r(seed) % (hi-low+1) + low;
}



int main(int argc, char *argv[]) {


	int numSpins ;		
	int	numWins, localx;
int  mystart,dist, myend, i,f , n,numproc, myid;
int x =0.0;
int rou = 0.0;		
clock_t startT, stopT; 


printf("Simulation d'une roulette américainne\n") ;
printf("Nbr tours         Nbr tour gagnant        pourcentage de gagner\n");


		
startT = clock(); 
 unsigned int seed = (unsigned) time(NULL);
	srand(seed);	



MPI_Init(&argc, &argv);	
MPI_Comm_size(MPI_COMM_WORLD, &numproc);
MPI_Comm_rank(MPI_COMM_WORLD,&myid);


	numSpins = 1;


	while (numSpins < MAX) {

n = numSpins;


mystart = (n / numproc) * myid;				// l'initiailsation des débuts et fins de chaque processus en dépant à nombre de tours 									distribués
  if (n % numproc > myid){
    mystart += myid;
    myend = mystart + (n / numproc) + 1;

  }else{
    mystart += n % numproc;
    myend = mystart + (n / numproc);
  } 


f=0;
dist = myend - mystart;					// le nombre des tours donnés pour chacun des processus 

    x =  getNumWins(dist, seed);

  

MPI_Reduce(&x, &rou, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);  // l'accumulation de nombre gagnants dans pour chaque processus pour afficher la 									nombre totale apres.




if (myid==0) {
showResults(numSpins, rou); }
	
 

numSpins =  numSpins * 2;	

x = 0;
rou = 0;
}

 MPI_Finalize();


	stopT= clock();		
double t = (stopT - startT)/CLOCKS_PER_SEC;

if (numSpins >= MAX ) printf("le temps d'execution %f", t);

	

	return 0;
} 




int getNumWins(int numSpins, unsigned int seed) {

	static int wins;
	int spin;		
	int myBet = 10; 

	wins = 0;	
	
#pragma omp parallel for num_threads(nThreads) default(none) \
    shared(numSpins, myBet) \
	private(spin, seed) \
	reduction(+:wins)
	for (spin=0; spin<numSpins; spin++){	 		// partagé le nombre des tours distribué déja a chaque processus 
	
		if (spinRed(myBet, &seed) > 0) 
			wins++;
	}
	
	return wins;
}  
int spinRed(int bet, unsigned int *seed) {
	int payout;
	int slot = rand_rIntBetween(1,38, seed);
	
	if (slot <= 18)
		payout = bet;	
	else if (slot <= 36) 
		payout = -bet;	
	else 
		payout = -(bet/2); 
	return payout;
} 


void showResults(int numSpins, int numWins){

	double percent = 100.0* (double)numWins/(double)numSpins;
	
printf("%d                  %d                          %f\n",numSpins,numWins,percent);
} 
