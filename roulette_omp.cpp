/* 	
	La simulation avec Openmp  a comme concept de Partager le nombre tour entre les differents treads 
*/

#include <iostream>
#include <omp.h>
#include <iomanip>
#include <time.h>
#include <string>
#include <stdlib.h>   
#include <stdio.h>     
using namespace std;

const int MAX = 1<<30;	
const int nThreads = 4; 
int getNumWins(int numSpins, unsigned int seed);
void showResults(int numSpins, int numWins);
int spinRed(int bet, unsigned int* seed);
int rand_rIntBetween(int low, int hi, unsigned int* seed) {
	return rand_r(seed) % (hi-low+1) + low;
}
int main() {

	int numSpins;		
	int	numWins;		



    double ompStartTime, ompStopTime;   
    int tid;      
    omp_set_num_threads(nThreads);  
    unsigned int seed = (unsigned) time(NULL);
	srand(seed);

	cout<<"Simulation d'une roulette américainne \n" <<
		string(35,'*')<<endl;
	cout<<setw(12)<<"Nbr tours    " << setw(12) <<"Nbr de parties gagnantes     " << setw(12) <<"% ""de gagner "<<endl;
	numSpins = 1;	
    ompStartTime = omp_get_wtime();
		
	while (numSpins < MAX) {
		numWins = getNumWins(numSpins, seed);	
		showResults(numSpins, numWins);				

		numSpins += numSpins;	
	} 
    ompStopTime = omp_get_wtime();
	
	cout<<"\nElapsed wall clock time: "<< (double)(ompStopTime - ompStartTime)<<endl<<endl;

	cout<<"\n\n\n\t\t*** Normal Termination ***\n\n";
	return 0;
}
int getNumWins(int numSpins, unsigned int seed) {

	static int wins;
	int spin;		
	int myBet = 10; 

	wins = 0;	
#pragma omp parallel for num_threads(nThreads) default(none) \
    shared(numSpins, myBet) \
	private(spin, seed) \
	reduction(+:wins)
	for (spin=0; spin<numSpins; spin++){ 		// partagé le nombre des tours distribué déja a chaque processus 

		if (spinRed(myBet, &seed) > 0)
			wins++;
	}	
	
	return wins;
}  
int spinRed(int bet, unsigned int *seed) {
	int payout;
	int slot = rand_rIntBetween(1,38, seed);
	
	if (slot <= 18) 
		payout = bet;	
	else if (slot <= 36)
		payout = -bet;	
	else 
		payout = -(bet/2); 
	return payout;
} 

string prettyInt(int n) {

	string s="";	
	int digit;		
	int digitCnt=0; 

	do {
		digit = n % 10;		
		n = n/10;			
		
		char c = (char) ( (int)'0' + digit);
		s.insert(0,1,c);	
		digitCnt++;			
		if ( (digitCnt%3 == 0) && (n>0) )
			s.insert(0,1,',');
	} while (n>0);
	return s;
} 
void showResults(int numSpins, int numWins){

	double percent = 100.0* (double)numWins/(double)numSpins;
	cout<<setw(12)<< numSpins << setw(12) << 
		numWins << setw(30) <<
		setprecision (4) << fixed << percent<< endl;
}
Said Scholes
