/* la simulation Hybride  avec Pthread et OpenMP, avc 8 Threads, et partagé le nombre des tours pour chaque thread

*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>

const int MAX = 1<<30;	
#define NTHREADS      8



int getNumWins(int dist);
void showResults(int numSpins, int numWins);
int spinRed(int bet );


double  sum=0.0;
pthread_mutex_t sum_mutex;

void *do_work( void *Y) 
{
    pthread_mutex_lock (&sum_mutex);
  int i, end,dist;
  int *nn;
  double x=0.0;
  nn = (int *) Y;  
dist = (*nn);
 


  x = 0.0;
  x =  getNumWins(dist);   // Nombre des tours gagnante
    
  sum = sum + x;
  pthread_mutex_unlock (&sum_mutex);
  pthread_exit(NULL);
}







int main(int argc, char *argv[]) {
  int i, start, tids[NTHREADS],n;
  pthread_t threads[NTHREADS];
  pthread_attr_t attr;
  clock_t startT, stopT; 
  startT = clock(); 
  int mystart,myend,dist;
 

  int numSpins ;		
  int	numWins, localx;
	



  printf("Simulation d'une roulette américainne\n") ;
  printf("Nbr tours         Nbr tour gagnant        pourcentage de gagner\n");


		
  startT = clock(); 
  srand(time(NULL));

 numSpins = 1;


while (numSpins < MAX) {

n = numSpins;

  pthread_mutex_init(&sum_mutex, NULL);
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
  for (i=0; i<NTHREADS; i++) {					// créer les Threads et le nombre des tours associés a chacun d'entre eux 
      mystart = (n / NTHREADS) * i;                
  if (n % NTHREADS > i){
    mystart += i;
    myend = mystart + (n / NTHREADS) + 1;

  }else{
    mystart += n % NTHREADS;
    myend = mystart + (n / NTHREADS);
  } 
dist = myend - mystart;
    pthread_create(&threads[i], &attr, do_work, (void *) &dist);
  }

  for (i=0; i<NTHREADS; i++) {
    pthread_join(threads[i], NULL);
  }


  showResults(n, sum);
 
numSpins =  numSpins * 2;
sum = 0;
}



  stopT= clock();	
	
  double t = (stopT - startT)/CLOCKS_PER_SEC;
  printf("le temps d'execution %f", t);

	

  return 0;
} 




int getNumWins(int dist) {

  static int wins;
  int spin;		
  int myBet = 10;
  int temp;

  wins = 0;	
#pragma omp parallel for num_threads(nThreads) default(none) \  // partagé le nombre des tours distribué déja a chaque Thread par autres
    shared(numSpins, myBet) \
	private(spin, seed) \
	reduction(+:wins)
	
  for (spin=0; spin<dist; spin++){
	
    temp = spinRed(myBet);
    if (temp>0) wins++;
  
	
  }

  return wins;
}  

int RandRange(int Min, int Max)
{
    int diff = Max-Min;
    return (int) (((double)(diff+1)/RAND_MAX) * rand() + Min);
}

int spinRed(int bet) {
  int payout;
  int slot;
slot  = RandRange(1, 38);  

  if (slot <= 18)
    payout = bet;	
  else if (slot <= 36) 
    payout = -bet;	
  else 
    payout = -(bet/2); 
  return payout;
} 



void showResults(int numSpins, int numWins){

  double percent = 100.0* (double)numWins/(double)numSpins;
	
  printf("%d                       %d                          %f\n",numSpins,numWins,percent);
} 