/* 	
	La simulation avec MPI et  PTHREAD, , le nombre de tour general est diviser sur les processus, et chaque processus à son tour divise son nombre de tours sur 8 Threads, et ces derniers font chacun son calcul
*/
#include <mpi.h>
#include <stdio.h>
#include <time.h>
#include <pthread.h>
    


const int MAX = 1<<30;	
#define NTHREADS      8


int getNumWins(int numSpins);
void showResults(int numSpins, int numWins);
int RandRange(int Min, int Max);
int spinRed(int bet);
void *do_work( void *y);
int rand_rIntBetween(int low, int hi, unsigned int* seed) {
	return rand_r(seed) % (hi-low+1) + low;
}


double  sum=0.0;
pthread_mutex_t sum_mutex;

void *do_work( void *Y) 
{
    pthread_mutex_lock (&sum_mutex);
  int i, end,dist;
  int *nn;
  double x=0.0;
  nn = (int *) Y;  
dist = (*nn);
 


  x = 0.0;
  x =  getNumWins(dist);   // Nombre des tours gagnante
    
  sum = sum + x;
  pthread_mutex_unlock (&sum_mutex);
  pthread_exit(NULL);
printf("SAlut");
}



int main(int argc, char *argv[]) {


	int numSpins ;		
	int	numWins, localx;
int  mystart,dist, myend, i,f , n,numproc, myid,tids[NTHREADS],np,mystartp,myendp,distp;
  pthread_t threads[NTHREADS];
  pthread_attr_t attr;

int x =0.0;
int rou = 0.0;		
clock_t startT, stopT; 


printf("Simulation d'une roulette américainne\n") ;
printf("Nbr tours         Nbr tour gagnant        pourcentage de gagner\n");


		
startT = clock(); 
 unsigned int seed = (unsigned) time(NULL);
	srand(seed);	



MPI_Init(&argc, &argv);	
MPI_Comm_size(MPI_COMM_WORLD, &numproc);
MPI_Comm_rank(MPI_COMM_WORLD,&myid);


	numSpins = 1;


	while (numSpins < MAX) {

n = numSpins;
pthread_mutex_init(&sum_mutex, NULL);
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

mystart = (n / numproc) * myid;                  // l'initiailsation des débuts et fins de chaque processus en dépant à nombre de tours TOtale
  if (n % numproc > myid){
    mystart += myid;
    myend = mystart + (n / numproc) + 1;

  }else{
    mystart += n % numproc;
    myend = mystart + (n / numproc);
  } 

f=0;
dist = myend - mystart;

np = dist;

for (i=0; i<NTHREADS; i++) {

mystartp = (np / NTHREADS) * i;                
  if (np % NTHREADS > i){
    mystartp += i;
    myendp = mystartp + (np / NTHREADS) + 1;

  }else{
    mystartp += np % NTHREADS;
    myendp = mystartp + (np / NTHREADS);
  } 
    distp = myendp - mystartp;
   
    pthread_create(&threads[i], &attr, do_work, (void *) &distp);
  }


for (i=0; i<NTHREADS; i++) {
    pthread_join(threads[i], NULL);
  }

MPI_Reduce(&sum, &rou, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);


if (myid==0) {
showResults(numSpins, rou); }
	
 

numSpins =  numSpins * 2;	

x = 0;
rou = 0;
}

 MPI_Finalize();


	stopT= clock();		
double t = (stopT - startT)/CLOCKS_PER_SEC;
printf("le temps d'execution %f", t);

	

	return 0;
} 



int getNumWins(int dist) {

  static int wins;
  int spin;		
  int myBet = 10;
  int temp;

  wins = 0;	

	
  for (spin=0; spin<dist; spin++){
	
    temp = spinRed(myBet);
    if (temp>0) wins++;
  
	}
  } 

int RandRange(int Min, int Max)
{
    int diff = Max-Min;
    return (int) rand()%(Max-Min)+Min;
}

int spinRed(int bet) {
  int payout;
  int slot;
slot  = RandRange(1, 38);  

  if (slot <= 18)
    payout = bet;	
  else if (slot <= 36) 
    payout = -bet;	
  else 
    payout = -(bet/2); 
  return payout;
} 


void showResults(int numSpins, int numWins){

	double percent = 100.0* (double)numWins/(double)numSpins;
	
printf("%d                       %d                          %f\n",numSpins,numWins,percent);
} 