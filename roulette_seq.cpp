

/* 	Une roulette américainne a 38 cases , 18 sont rouges et 18 sont noire et 2 sont verts
	Quand une personne parit soit sur le rouge ou le noir, la probabilité de gagner est de 18/38 voir 47.38%

	la simulation de la roulette consiste a faire des tours de 2 juska 2^20 et dans chaque ensemble de tours on est soit gagnant soit 		perdants
*/

#include <iostream>
#include <iomanip>
#include <time.h>
#include <string>
#include <stdlib.h>    
using namespace std;

const int MAX = 1<<30;	



int getNumWins(int numSpins, unsigned int seed);
void showResults(int numSpins, int numWins);
int spinRed(int bet, unsigned int* seed);
int rand_rIntBetween(int low, int hi, unsigned int* seed) {
	return rand_r(seed) % (hi-low+1) + low;
}

int main() {
	
	int numSpins;		
	int	numWins;		
	clock_t startT, stopT; 
	startT = clock();    
    unsigned int seed = (unsigned) time(NULL);
	srand(seed);	

	cout<<"Simulation d'une roulette américainne \n" <<
		string(35,'*')<<endl;
	cout<<setw(12)<<"Nbr tours    " << setw(12) <<"Nbr de parties gagnantes     " << setw(12) <<"% ""de gagner "<<endl;
	numSpins = 1;					
	while (numSpins < MAX) {					// Faire tourner la roulette plusieurs tours a chaque fois
		numWins = getNumWins(numSpins, seed);	 		/* Simuler le nombre des tours gagnants dans l'ensemble des tours a 										chaque fois*/
		showResults(numSpins, numWins);				
		numSpins += numSpins;	
	} 
	stopT= clock();		
	cout<<"\n temps d'execution: "<< (double)(stopT-startT)/CLOCKS_PER_SEC<<endl<<endl;

	return 0;
}
int getNumWins(int numSpins, unsigned int seed) {      // Fonction qui fait la simulation en entrant le nombre des tours et fait appelle a une 								autre fonction qui génére par la suite des échantillons aléatoire entre 1 et 38, qui 								représentent ce qu'on obtient  a chaque tour  											

	static int wins; 
	int spin;		
	int myBet = 10;

	wins = 0;	
	
	for (spin=0; spin<numSpins; spin++){
		
		if (spinRed(myBet, &seed) > 0) 
			wins++;
	}
	
	return wins;
}  
int spinRed(int bet, unsigned int *seed) {
	int payout;
	int slot = rand_rIntBetween(1,38, seed);
	
	if (slot <= 18)
		payout = bet;	
	else if (slot <= 36)
		payout = -bet;	
	else 
		payout = -(bet/2);
	return payout;
} 
void showResults(int numSpins, int numWins){

	double percent = 100.0* (double)numWins/(double)numSpins;
	cout<<setw(12)<<numSpins<< setw(12) << 
		numWins << setw(30) <<
		setprecision (4) << fixed << percent<< endl;
}
